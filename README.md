## Excerpt
Create a program that can classify a random sequence of integers according to the number of ranges each number is included in.
Let's define MaxValue to be 1,000,000,000
A range is comprised of two values (min, max) where: min < max, min >= 0 and max <= MaxValue
The program should generate N random ranges
The program should also generate M random integers between 0 and MaxValue
For each random integer I, the program should calculate how many of the N ranges, fully enclose the value.
A number I is enclosed by a range (min, max) when: (I >= min) && (I < max)
For each random integer I, the program should then output a line to stdout of the form:
I => Enclosed by K range(s)
## Requirements
* The value N is to be specified as a command line argument. Default value should be 1,000,000
* The program will generate random numbers infinitely until the program exits
* Pressing Q should exit the program - ensure clean shutdown.
* The program is time-critical - calculating the number of ranges should be as efficient as
possible.
### Concrete example
N=3
Ranges = {10, 60}, {0, 90}, {95, 102} Random Integers = 65, 11, 0, 94, 50, ... 
### Result:
* 65 => Enclosed by 1 range(s)
* 11 => Enclosed by 2 range(s)
* 0 => Enclosed by 1 range(s)
* 94 => Enclosed by 0 range(s)
* 50 => Enclosed by 2 ranges(s)
## Explanation
* 65 is enclosed by {0, 90} only
* 11 is enclosed by {10, 60} and {0, 90} 0 is enclosed by {0, 90}
* 94 is enclosed by nothing
* 50 is enclosed by {10, 60}, {0, 90}
## Success Criteria
1. The entire solution should be in a single code file. No need to worry about build instructions.
2. The candidate should write code to solve this using one of the following languages: C++, C#, Go, Typescript, Javascript.
• Write the solution as if it were production code.

## Instructions

### To install the app run the following command within the root directory of the app (latest nodejs 10.8.0)

```sh
npm install
```

### To start the app with arguments(N Number of ranges):

```sh
npm start -- 100
```

### To start the app with default number of ranges:

```sh
npm start
```

### To start the app:

```sh
npm start
```

### To run unit tests:

```sh
npm test
```

### To build the app:

```sh
npm run build
```

### To run the tests in watch mode:

```sh
npm run tdd
```

### To lint the app:

```sh
npm run lint
```

### To quit the app press 'q'

## Comments

It was interesting to remind myself functional recursive patterns like trampolining, continues passing style and async thunks, although the resulting code may not be optimal.

I guess javascript's lack of tail call optimisation doesn't make it the best candidate for recursion; hopefully new engines will support TCO.

Although lacking clear architectural pattern, the code contains simple and clean functions.

Even though not extensive, the existing tests are enough to help identify inconsistencies and bugs especially when cranking up the samples.

Regarding the generation of ranges - a small experiment generating 5000 unique ranges took 17s. So I guess 1B would take 39 days.
I am not sure how I would approach this problem in javascript (single threaded environment), but potentially by partitioning the workload and offloading it to child processes using a thread-safe cash. Another option could be using readable pipable stream, reading from it in chunks and keep processing/printing the results, rather than waiting for the full list of ranges.

My approach for the algorithm that checks if a number belongs in a range
was standard brute force. A binary tree would be more efficient but I didn't know the implementation off the top of my head and wouldn't be able to complete the task within a reasonable time limit.

I chose gitlab as it enables to quickly develop in a [CI/CD](https://gitlab.com/l.papazianis/ef-interview/pipelines) fashion but also create an [issue board](https://gitlab.com/l.papazianis/ef-interview/boards?=) to keep track of workload.

Overall time spend around 5-6 hours of non-continuous work.
