import { getRanges, isInRange, MAX, RangesDictionary } from './index';

jest.setTimeout(300000000);

describe('getRanges', () => {
	describe('Given 3 as number of ranges', () => {
		test('It should return 3 number ranges', async () => {
			const noOfRanges: number = 3;
			const ranges = await getRanges(noOfRanges);
			expect(Object.keys(ranges).length).toEqual(noOfRanges);
		});
	});
	describe('Each range', () => {
		test('Should be have a min >= 0 and a max <= number of ranges specified', async () => {
			const noOfRanges: number = 3;
			const ranges: RangesDictionary = await getRanges(noOfRanges);
			Object.keys(ranges).forEach((key: string) => {
				const min = ranges[key].min;
				const max = ranges[key].max;
				expect(min >= 0).toBeTruthy();
				expect(min < max).toBeTruthy();
				expect(max <= MAX).toBeTruthy();
			});
		});
	});
	describe('InRange', () => {
		test('20 should be between the range 10 and 40', () => {
            const x: number = 20;
			expect(isInRange(x, {min: 10, max: 40})).toBeTruthy();
		});
	});
});
