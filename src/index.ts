if (process.stdin.setRawMode) {
    process.stdin.setRawMode(true);
}
process.stdin.setEncoding('utf8');

export const MAX = 1000000000;

export interface Range {
	min: number;
	max: number;
}

export interface RangesDictionary {
	[key: string]: Range;
}

/**
 * Trampoline for recursive functions, alternative to CPS, cleaner and meaner (avoid callback hell). Cleaner way
 * for recursive functions and as there is no tail call optimization in js it serves the purpose
 * @param fn
 */
const trampoline = (fn: Function) => (...args: any[]) => {
	let result: any = fn(...args);
	while (typeof result === 'function') {
		result = result();
	}
	return result;
};

/**
 * It generates a random number within a range, by default it includes the max and the min.
 * A validator array can be passed in. Caution the smaller the MAX
 * the more prone to falling into an endless while as the sample list becomes smaller.
 * @param min
 * @param max
 * @param inclusive
 * @param exclusions
 */
export function getRandomIntRec(
	min: number,
	max: number,
	inclusive: boolean = true,
	exclusions: number[] = []
): number | Function {
	const args = Array.from(arguments);
	const seed = Math.random();
	const ceiledMin = Math.ceil(min);
	const flooredMax = Math.floor(max);
	const bounce = () => getRandomIntRec.apply(undefined, args);
	const ifExists = (x: number) => (y: number) => x === y;
	const incNo = inclusive ? 1 : 0;
	const result =
		Math.floor(seed * (flooredMax - ceiledMin + incNo)) + ceiledMin;
	const shouldBounce = exclusions.length && exclusions.find(ifExists(result));
	return shouldBounce ? bounce : result;
}

/**
 * Trampolined random number generator
 */
const getRandomInt = trampoline(getRandomIntRec);

/**
 * Checks if a number is within a given range
 * @param  n  number
 * @param  range Range
 * @return boolean
 */
export const isInRange = (n: number, range: Range): boolean =>
	n >= range.min && n < range.max;

/**
 * Generates in an async fashion a large number of ranges. It operates in CPS style employing essentially callbacks
 * to avoid stack overflow
 * @param remaining
 * @param ranges
 * @param cpsCb
 */
function generateRanges(
	remaining: number,
	ranges: RangesDictionary,
	cpsCb: any
): any {
	const args = Array.from(arguments);
	const min = getRandomInt(0, MAX);
	const max = getRandomInt(min, MAX, true, [min]);
	const key = `${min}|${max}`;
	const alreadyExists = ranges[key];
	const newCpsCb = (ranges: RangesDictionary) => cpsCb({ ...ranges, [key]: { min, max } });
	const end = () => cpsCb({ ...ranges });
	const retry = generateRanges.bind(undefined, ...args);
	const newRanges = { ...ranges, [key]: { min, max } };
	const newArgs = [remaining - 1, newRanges, newCpsCb];
	const next = generateRanges.bind(undefined, ...newArgs);
	const isFinal = remaining === 0;
	return isFinal
		? end()
		: alreadyExists
			? setTimeout(retry)
			: setTimeout(next);
}

/**
 * Public api for generating ranges it can be used either as a promise chain or via async/await.
 * The number of ranges can be specified or it will default to the MAX constant
 * @param max
 */
export const getRanges = (max: number = MAX): Promise<RangesDictionary> =>
	new Promise(resolve => generateRanges(max, {} as RangesDictionary, resolve));

/**
 * Starts listening in the standard input for keystrokes and emits via the observer
 */
const listenForUserInput = () => {
	process.stdin.on('readable', () => {
		const chunk = process.stdin.read();
		if (chunk == 'q') {
			process.exit(0);
		}
	});
};
/**
 * Returns a concatenated string of ranges
 * @param ranges
 */
const formatRangeText = (ranges: Range[]) => ranges
	.map((range: Range) => `{${range.min}, ${range.max}}`)
	.join(', ');

/**
 * Forms the log message to be printed out;
 * @param number
 * @param ranges
 */
const formatter = (number: number, ranges: Range[]) =>
	`${number} is enclosed by  ${ranges.length} range(s) ${ranges.length ? '-> ' + formatRangeText(ranges) : ''}\n`;

/**
 * Main program
 * @param ranges
 */
const program = (ranges: RangesDictionary) => () => {
    const randomNumber = getRandomInt(0, MAX);
    const incRanges = Object
        .keys(ranges)
        .map((key) => ranges[key])
        .filter((range: Range) => isInRange(randomNumber, range));
    console.log(formatter(randomNumber, incRanges));
};

/**
 * Main entry point
 */
export const main = async () => {
	const numberOfRanges = Number(process.argv[2] || MAX);
    listenForUserInput();
	const ranges = await getRanges(numberOfRanges);
	setInterval(program(ranges), 20);
};

main();